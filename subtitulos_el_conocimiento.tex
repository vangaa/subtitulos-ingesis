\documentclass[12pt, letterpaper]{article}

\usepackage[headheight=4cm, top=4cm, left=4cm, bottom=2.5cm, right=2.5cm]{geometry}
\usepackage[scaled]{helvet} %arial es una versión privativa de helvética
\usepackage[spanish]{babel} 
\usepackage[utf8]{inputenc}

\begin{document}

\begin{quotation}
\noindent \em El camino de la vida puede ser libre y hermoso. Pensamos demasiado
y sentimos muy poco. El odio de los hombres pasará y caerán los dictadores y el 
poder que le quitaron al pueblo se le reintegrará al pueblo y así, mientras el 
hombre exista, la libertad no perecerá. 
\end{quotation}

Ver, saber. Pensar. Cuando la filosofía se pregunta por el sentido de las
cosas, pone en juego dos cuestiones: por un lado, qué es lo real. Pero, al 
mismo tiempo, cómo lo conocemos. Qué es lo real. Cómo lo conocemos. Sin 
embargo, la pregunta por lo real opaca a la pregunta por el conocimiento. 
Cuando nos preguntamos por lo real estamos dando por supuesto un cierto tipo de 
conocimiento que, sin embargo, no evidenciamos. Cuando vemos algo, no nos 
estamos preguntando por cómo es que lo vemos, no estamos inquiriendo sobre la 
mirada o la naturaleza de los ojos. Solo nos concentramos en lo que vemos. Toda 
definición de lo real se inicia con una confianza fundamental. Damos por 
evidente un cierto tipo de conocimiento. Por eso la pregunta por el 
conocimiento es una pregunta, desde el principio, problemática. Ya que pone en 
cuestión la herramienta misma con la que emprendemos la pegunta. Mis ojos 
pueden, en principio, ver cualquier cosa, pero lo que no pueden ver es a ellos 
mismos mirando. ¿Es posible conocer el modo en que los seres humanos conocemos? 
es tarea de la gnoseología. 


\section{Estudio del conocimiento}

En filosofía llamamos ``ontología'' al estudio del ser en tanto ser y 
``gnoseología'' al modo en cómo accedemos al ser. En vez de poner el acento en 
lo real, ponemos el acento en cómo el hombre se relaciona con lo real para 
conocerlo. Pero ¿no se convertiría, entonces, lo real solo en el modo en que lo 
conocemos? Un primer problema de la gnoseología es poder delimitar cuándo algo 
en lo que creemos merece llamarse conocimiento. ¿Existen conocimientos válidos 
y conocimientos inválidos? Pero, sobre todo, ¿qué se juega en la necesidad de 
tener que establecer ciertos saberes como válidos? ¿Es solo el deseo de querer 
alcanzar la verdad? La gnoseología busca establecer las condiciones que hacen 
de un conjunto de afirmaciones un conocimiento válido. No le importa qué es lo 
que se conoce, sino cómo lo justificamos, no si esto es una cama, sino cómo 
sabemos que es una cama. Todo conocimiento debe seguir un método cuya clave es 
la puesta a prueba de cualquier afirmación. Todo debe ser puesto a prueba. 
Pero ¿cuándo una prueba es legítima? ¿No tiene también la prueba que ser puesta 
a prueba? La puesta a prueba debe satisfacer ciertos rasgos fundamentales, como 
la objetividad, la neutralidad o la demostración, que hoy solo los cumple el 
conocimiento científico. 
 
En los últimos tiempos, la ciencia se ha constituido en un saber con 
pretensión de verdad hegemónico y ha relegado al ámbito de lo personal, de lo 
mágico, de lo irracional al resto de los saberes. ¿Esto significa que el arte y 
la religión, por tomar dos ejemplos, no son estrictamente conocimientos 
válidos? Pero ¿qué es el conocimiento? ¿Hay formas de conocer más válidas que 
otras?. 

Tomemos cuatro formas distintas del conocer: la razón, los sentidos, la fe y la 
intuición. ¿Existe alguna definición de conocimiento que albergue esta 
diversidad, que encuentre algo común entre todas? La respuesta es negativa. 
Estas cuatro formas de conocer tuvieron, en nuestra cultura, recorridos 
diferentes. El saber científico solo aceptó como conocimiento válido a aquel 
que se haya justificado en la razón o en los sentidos y descartó tanto a la fe 
como a la intuición. Podemos intuir que algo está por pasar o tener fe en 
alguna convicción religiosa. Pero, claramente, no se trata de conocimiento. 
 
\section{Formas de entender el conocimiento}

Pero, entonces, ¿qué es el conocimiento? Empecemos por lo más evidente. Toda 
relación de conocimiento se nos presenta clásicamente como una relación entre 
un sujeto y un objeto. Sujeto es el que conoce, objeto, lo que es conocido.
El sujeto es claramente el hombre, mientras que el objeto es todo aquello que, 
supuestamente, está fuera de él, incluyendo, paradójicamente, al hombre mismo. 
El hombre es, al mismo tiempo, sujeto y objeto. Pero ¿cómo conoce el sujeto al 
objeto? ¿Se trata de un conocimiento individual o colectivo? ¿Quién hace al 
conocimiento? ¿Individuos, libros, especialistas, comunidades? ¿Es el acto de 
conocimiento pasivo o activo? ¿Reflejamos nuestro objeto de conocimiento o lo 
construimos? Empecemos por la cuestión de la posibilidad misma del 
conocimiento. ¿Puede el sujeto alcanzar al objeto? ¿Es posible el 
conocimiento? Todo indicaría que sí. 
 
\subsection{Dogmatismo}

Por un lado, tenemos al dogmatismo, que es una postura que entiende que el 
conocimiento no solo es posible, sino que es obvio y evidente, pero para eso 
tiene que anclarse en alguna certeza que nunca cuestiona. El dogmatismo da por 
sentada la posibilidad del conocimiento. No lo ve como una relación, sino que 
entiende que el objeto se presenta a un sujeto, que solo lo recibe y lo 
representa. El componente dogmático está en la confianza ciega puesta en el 
acto de conocimiento. Un dogma se presenta en la medida en que se ausenta el 
pensamiento crítico, en la medida en que no hay problematización sobre lo que 
se conoce. Es como si el objeto viniese ya con rasgos predeterminados y el 
sujeto fuese un mero representador de aquello que se me presenta. Pensar que 
todo lo que se nos da se da por algo y que ese algo debe estar bien es la 
esencia misma del dogmatismo. Lo dogmático está en aceptar la plataforma de 
verdades que muestra el objeto sin cuestionar nada porque, en definitiva, 
alguien nos presenta esa plataforma. Pero ¿es realmente posible no partir de 
ningún dogma? ¿No estamos siempre dando por supuestas algunas verdades para 
comenzar a pensar en cualquier cosa? 
 
\subsection{Escepticismo}

Por otro lado, tenemos al escepticismo, que niega la posibilidad del 
conocimiento. Si el dogmatismo prioriza al objeto y minimiza al sujeto, el 
escepticismo realiza el movimiento contrario. El conocimiento se concentra 
tanto en el sujeto, que pierde por completo al objeto. Para el escepticismo, 
cuando conocemos, no podemos desvincularnos de nuestras circunstancias: la 
cultura, la época, la familia, nuestros valores, nuestros propios miedos. Por 
eso todo acceso a lo real está siempre mediado por alguna categoría subjetiva. 
Pero ¿se puede, entonces, seguir hablando de conocimiento? Para el escepticismo 
no es posible el conocimiento. Todo es cuestionable, nada cierra de modo 
absoluto. No hay manera de demostrar que lo que conocemos sea algo definitivo 
porque el sujeto lo está siempre distorsionando. Así se vuelve imposible hablar 
de un objeto en sí mismo. No hay conocimiento del objeto, sino conocimiento de 
los modos en que el sujeto conoce al objeto. Si no es posible afirmar un 
conocimiento absoluto, ¿vale algún tipo de saber? ¿O nada tiene sentido?.
 
Hay algo de interesante, sin embargo, en el escepticismo. Una cosa es negar 
todo conocimiento y otra desconfiar de los objetos que se me presentan 
ejerciendo una actitud crítica y de sospecha frente a los valores dominantes. 
En ese sentido, hay algo de escéptico en todo saber. 
 
¿Dogmatismo o escepticismo? La discusión sobre la posibilidad del conocer nos 
lleva al problema del origen y validez del conocimiento. 
 
\section{Como adquirimos conocimiento}

¿Dónde se origina, en el ser humano, el conocimiento? ¿Por dónde ingresa el 
conocimiento al sujeto? Hay dos respuestas clásicas que dominaron el debate en 
los inicios del pensamiento moderno: los sentidos o la razón, empirismo o 
racionalismo. 

\subsection{El racionalismo}

El racionalismo va a sostener que el único conocimiento válido es el racional, 
en la medida en que garantiza dos rasgos claves que todo conocimiento tiene que 
tener para ser válido: \textbf{necesidad lógica} y \textbf{validez universal}. 
 
Necesidad lógica significa que es independiente de la experiencia. Por 
ejemplo, que el todo sea mayor que sus partes no se valida en ninguna 
comprobación concreta, sino que vale a priori. No surge de los hechos, sino que 
los hechos se ordenan según esta lógica. Y además estamos hablando de una 
verdad universal ya que vale para todos los casos posibles sin excepción. 
Necesidad lógica y validez universal son dos principios que se encuentran 
claramente en las matemáticas, la ciencia modelo por excelencia para el 
racionalismo. Obviamente, el racionalismo no niega el conocimiento empírico, 
pero lo coloca en el lugar del error. Los sentidos engañan. Generan una 
relación primaria con la realidad, pero necesitan de la razón para ser 
encauzados y puestos en su justo lugar. Para el racionalismo hay ideas 
innatas. Nuestra mente no solo no viene vacía, sino que viene provista de 
información verdadera. Dependiendo de la época y de los autores, el estatus de 
estas ideas innatas va modificándose: en Platón son metafísicas, en San Agustín 
son divinas y, a partir de Descartes, Leibniz y Spinozza, se van justificando 
con la ciencia emergente. Si el racionalismo puede garantizar verdades innatas 
y a priori, necesarias y universales, lo hace a costa de abandonar la presencia 
de la realidad material. A veces se tiene la impresión de que solo funcionaría 
bajo una metafísica que haga de nuestra razón algo desvinculado por completo 
del cuerpo. Pero ¿se puede hacer un planteo tan tajante? ¿Se puede excluir así 
la experiencia de los sentidos? O peor, ¿se puede seguir sosteniendo una 
metafísica tan absoluta después de los cambios científicos que comienzan a 
darse en la modernidad? 
 
\subsection{El Empirismo}

Frente al racionalismo, que postula a la razón, surge el empirismo, que 
prioriza los sentidos. Pero la crítica a la razón que se inicia con el 
empirismo, va a conducir, a la larga, a poner en cuestionamiento al mismo 
sujeto que conoce. En definitiva, ¿se puede seguir pensando al conocimiento 
solo como una relación neutral entre un sujeto y un objeto? El principio 
fundamental del empirismo es que todos nuestros conocimientos provienen y se 
validan en la experiencia. 

Nuestra mente es como una tabula rasa o un papel en blanco que vamos rellenando 
con información a medida que vamos produciendo conocimiento sensible. Todo lo 
que sabemos proviene de experiencias empíricas concretas. Por ejemplo, saber si 
el fuego quema o no quema, puede consistir en la advertencia de alguien, en la 
lectura de un libro o en el desarrollo de una teoría química sobre el origen de 
las quemaduras. Sin embargo, el conocimiento más enfático, el más contundente e 
indiscutible, va a estar ligado a la vivencia de una experiencia singular: al 
día en que nos quememos con fuego. 

\subsubsection{David Hume}

Para el empirismo de David Hume, es la vivacidad de la percepción lo que define 
la verdad del conocimiento y no hay percepción más vivaz que la que 
experimentamos por medio de los sentidos. Hume divide a las percepciones en dos 
categorías: impresiones e ideas. Una impresión es todo conocimiento empírico, y 
una idea es la copia de esa impresión que realizamos en la mente. 

El pilar del planteo empirista consiste en pensar que todas nuestras ideas 
provienen, en definitiva, de las impresiones sensibles. No hay saber innato ni 
saberes trascendentes. Si pienso en un cuadro, es porque primero observé un 
cuadro. Si tengo la idea de una cama, es porque primero la vi. Pero ¿qué pasa 
con la idea de un unicornio, por ejemplo? ¿De dónde la saqué?. Hume explica 
que allí la mente operó uniendo dos ideas separadas que provienen de dos 
impresiones separadas: un caballo y un cuerno. Nada hay en el unicornio que no 
provenga de la experiencia. 

Sin embargo, hay algo que todavía no funciona. Si la única manera de elaborar 
una ciencia se encuentra en la promulgación de leyes universales con el fin de 
predecir y explicar el sentido de las cosas que hay en el mundo, ¿no nos 
condena el empirismo a la tiranía del aquí y ahora?. La generalización se 
convierte en la base de nuestro conocimiento, pero es un tipo de razonamiento 
en el que no hay garantía de validez. Y, por otro lado, hemos dado por 
supuesto que nuestro acceso a la empiria es inmediato y transparente. Pero ¿es 
realmente así? ¿Recibimos de la realidad, pasivamente, sus manifestaciones? ¿O 
cuando la encaramos lo hacemos ya siempre desde algún marco ... siempre desde 
alguna postura previa?. 

\subsubsection{Kant}

Es clara la hegemonía de los hechos, pero ¿hay un orden en los hechos? ¿O hay, 
en el ser humano, un orden previo donde los hechos encajan? Se suele usar el 
concepto de "revolución copernicana" para ejemplificar el momento en que una 
propuesta produce un giro tal que rompe el plano mismo en el que se hallaba 
sentada. Kant genera una ruptura que ya se venía, de alguna manera, 
produciendo, pero definitivamente coloca el problema del conocimiento en otro 
lado. ¿Qué plantea Kant? El acto de conocimiento, además de ser una relación 
entre sujeto y objeto, es una relación activa, esto es, el sujeto no recibe y 
refleja al objeto cuando lo conoce, sino que lo produce. En la realidad hay 
algo, no sabemos qué, algo aún muy difuso e incomprensible, datos sin formas y 
sin orden que el sujeto tiene que elaborar. No hay una realidad en sí, sino 
que es el sujeto el que la constituye. El sujeto encara la realidad como si 
tuviera moldes en la mente y en el cuerpo, como si el hombre naciera con 
anteojos azules pegados a los ojos que nos condicionan a ver las cosas mucho 
más azuladas de lo que realmente son. No hay para Kant, al igual que para 
Hume, causalidad o sustancia en las cosas, sino que se trata de categorías con 
las que el sujeto ordena lo real. Un sujeto ahora activo. Podemos decir que 
Kant pretende colocarse más allá del empirismo y del racionalismo y esta 
diferencia la encontramos en la distinción que hace entre pensar y conocer. 
 
\begin{quotation}
\noindent ``Si mis molduras operan por sí solas, puedo pensar cualquier cosa,
pero el verdadero acto de conocimiento se produce cuando se aplican sobre 
una realidad empírica concreta.''
\end{quotation}

Así a Dios, al alma o al mundo los puedo pensar, pero nunca conocer. Pensar es 
más abarcativo que conocer, pero el conocimiento es más riguroso.

\subsection{Pensamiento Cientifico}

Hay una renuncia de Kant al conocimiento de lo absoluto y hay, en ese mismo 
acto, una decisión de apostar a la ciencia como único conocimiento confiable. 
La razón establece los marcos y los sentidos comprueban. La ciencia moderna 
parece seguir ese camino y se va a ir constituyendo así, en el paradigma del 
conocimiento mismo. Pero ¿qué sucede cuando un único tipo de saber se vuelve 
hegemónico? ¿Qué ganamos y qué perdemos con la ciencia como único modelo válido 
de conocimiento? Hablamos de un siglo XX con una notable hegemonía de la 
tecnociencia y la impugnación de otros saberes posibles. 

¿Qué es la tecnociencia? Es que la ciencia y la tecnología se vuelven los 
únicos saberes legítimos para la construcción del sentido válido de lo real. 
La ciencia y la tecnología, en la cultura occidental, definen hoy las verdades 
de época, pero también es cierta la emergencia de otros tipos de saberes que se 
van construyendo desde las fisuras de la tecnociencia. 

La falta de democratización del conocimiento, sus efectos perniciosos en la 
naturaleza, su uso y abuso en la destrucción de lo humano y de todo lo viviente 
van marcando agujeros por donde los saberes excluidos se cuelan. Hay otras 
tradiciones del saber que por fuera del modelo científico proveen sentido e 
incluso eficiencia al desarrollo de la vida en el mundo. 

Los saberes tradicionales de todo tipo, las religiones, el arte, la astrología, 
los saberes orientales son ejemplos de contraconocimientos, que, no en su 
totalidad, pero sí de modo parcial, brindan también soportes para una 
existencia oprimida por la tecnociencia. 

\subsubsection{Heidegger}

En Heidegger encontramos un planteo de disolución de la relación entre el 
sujeto y el objeto, que en el pensamiento posmoderno se conoce con el nombre de 
"la muerte del sujeto". El ser se abre a lo humano de diversas maneras. El 
conocimiento es una de las tantas formas, pero no es la única ni la más 
verdadera. Tanto el sujeto como el objeto son una construcción. No hay un 
hombre que conoce al ser, sino que el ser se manifiesta como una relación entre 
sujeto y objeto. Cuando pensamos al conocimiento como una relación entre un 
sujeto y un objeto, pensamos una determinada manera de darse el conocimiento, 
que, sin embargo, no ha sido la única ni lo seguirá siendo. 

Está claro que el conocimiento es una elaboración de lo humano, pero la idea 
misma de hombre, con todas sus características esenciales y como sujeto que 
conoce también es una elaboración. Se produce una tensión que, en principio, 
desplaza al hombre del centro, descentra al conocimiento y habilita la 
afluencia de saberes diversos.

\subsubsection{Thomas Kuhn}

En el pensamiento científico, también se produce una revolución conceptual a 
partir de la obra de Thomas Kuhn. Kuhn resignifica la idea de paradigma y 
renueva las formas en que la ciencia se venía pensando a sí misma. 

Para Kuhn no hay un progreso hacia la verdad, sino que los cambios en la 
historia de la ciencia han tenido que ver con cuestiones extracientíficas. La 
comunidad de especialistas y sus instituciones proclaman un paradigma 
científico e intentan, denodadamente, sostenerlo, ya que en él se juegan sus 
propios intereses. Por ello no hay un afuera del paradigma, sino que el afuera 
supone estar excluido de la misma categoría de conocimiento. 

La verdad es también una construcción del paradigma. Kuhn nos explica cómo 
nuestras lecturas de lo real están siempre previamente articuladas por una 
teoría. No hay accesos directos a la empiria, sino que hay una carga teórica de 
la observación, o, en otras palabras, uno encuentra lo que ya sabe que está 
buscando. 

Las ideas de Kuhn nos conducen a comprender el carácter político del 
conocimiento, que, sin embargo, se nos presenta como una herramienta que busca 
desinteresadamente la verdad. 

Pero quien decide sobre el conocimiento decide. Siempre la cuestión del saber 
se nos presenta como una cuestión de poder. Hoy vivimos el ocaso de una alianza 
histórica entre la ciencia y nuestra cultura, amparada por nuestras 
instituciones y naturalizada en las prácticas cotidianas. 

Romper con los monopolios del saber parece ser la única forma posible de 
resistencia. Conocer es básicamente una apuesta por lo nuevo, una convicción de 
apertura. No se trata de criticar a la ciencia para encerrarnos en otros 
dogmas, sino de salir de todo dogmatismo y seguir apostando por un saber 
abierto que siga reinventando una vez más lo humano, en especial en tiempos 
donde parece haber llegado a su fin la idea de hombre tal como la conocemos. O, 
como dice Foucault, borrándose, como en los límites del mar, un rostro de 
arena. 

\end{document}
