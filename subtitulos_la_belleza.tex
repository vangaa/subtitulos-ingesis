\documentclass[12pt, letterpaper]{article}

\usepackage[headheight=4cm, top=4cm, left=4cm, bottom=2.5cm, right=2.5cm]{geometry}
\usepackage[scaled]{helvet} %arial es una versión privativa de helvética
\usepackage[spanish]{babel} 
\usepackage[utf8]{inputenc}
\usepackage{setspace}
%\renewcommand{\baselinestretch}{1.5} %interlineado 1.5

\begin{document}

Lindo. Feo. Hermosura. Sublime. Todo lo que nos rodea genera en nosotros una 
reacción estética. Además valorar a las cosas como verdaderas o falsas, buenas 
o malas, las cosas también se nos presentan como bellas o feas. Durante mucho 
tiempo, y en sintonía con un mundo ordenado, la belleza era un valor claro, 
delimitado, objetivo e indiscutible. Hoy, en una época de desdiferenciación, 
fragmentación e hibridación, los límites de lo bello también se han 
transformado. ¿Qué es la belleza hoy? La más importante cuestión filosófica con 
respecto a la belleza, es el problema de su objetividad. El problema es 
determinar si la belleza está en las cosas o es relativa a quien la 
experimenta. En otras palabras, ¿es la belleza objetiva o subjetiva? Pero en 
nuestros tiempos se agrega otra problemática, ya que la belleza deja de tener 
que ver con un ámbito tan específico como el del arte, y se vuelve, en una 
sociedad hiperconsumista, un criterio estructural. 

En el mundo de hoy, todo es belleza. Nuestra existencia se ha estetizado. Los 
antiguos definían a la belleza con la fórmula de la proporción entre las 
partes, por eso, la belleza siempre estuvo ligada a la armonía y a la simetría. 
Si la belleza está en las cosas, y es proporción entre las partes, entonces, el 
hombre la puede medir objetivamente, y sin embargo, ¿por qué hoy nos resulta 
más bella Jimena que esta vasija? 

Pensemos la cuestión desde otra perspectiva.  Es claro que, en cada contexto, 
hay un criterio de lo bello que se impone como mayoritario. La belleza no está 
en las cosas pero tampoco depende de cada uno.  Hay criterios que se van 
estableciendo en cada época, o en cada cultura. Pero ¿a partir de qué? A esto 
se lo llama {\bf ``relativismo estético''}. La diferencia entre la belleza 
natural y la artificial es bastante ambigua.  Podríamos decir que, en tanto hay 
una intención de producción de belleza para su contemplación, entramos al 
ámbito del arte. 
 
De hecho, en la modernidad, son dos las definiciones de arte más importantes:

\begin{itemize}
	\item El arte es la representacion de lo real
	\item El arte es la produccion de la bello
	
\end{itemize}

Pero también es cierto que, a comienzos del siglo XX, las vanguardias 
artísticas aparecen para desestructurar estas dos definiciones, ya que 
cuestionan la entidad tanto de la realidad como de lo bello. Las vanguardias 
vienen a proponer que cada acto de nuestra existencia se vuelva más bello.  
Vienen a gritar que hay que hacer de nuestra vida cotidiana un acto creativo 
permanente. Hay que transformar nuestra vida cotidiana en algo bello. 

{\bf El vanguardismo} llevó al extremo las condiciones de ruptura frente a una 
sociedad que, a principios del siglo XX, se volvía cada vez más gris. De lo que 
se trata, ahora, es de salirse de los museos. Esto es, de rebelarse contra las 
instituciones que escinden al arte de la vida, que elitizan el quehacer 
artístico. Pero con estos gestos vanguardistas, más que reconciliarse con la 
vida, el arte se vuelve a alejar, y la belleza parece reservada a solo unos 
pocos especialistas. Pero entonces, ¿cómo hacemos de nuestra vida algo más 
bello? Hay dos maneras. Una ligada al consumo y a la mercantilización de lo 
bello. Donde embellecer la vida cotidiana pareciera reducirse a comprarnos 
ropa, accesorios, cambiarnos el peinado o el color de pelo, o tatuarnos el 
cuerpo. Embellecer nuestra vida cotidiana ¿es estar pendientes todo el tiempo 
de nuestra imagen? 

\begin{description}

\item[Nietzsche] sostiene que una estética de la existencia supone un ejercicio 
de creatividad permanente. En un mundo sin verdades absolutas, nos estamos  
recreando todo el tiempo a nosotros mismos, y cuanto más experimentamos lo  
diferente, más crecemos. Nietzsche sostiene que una estética de la existencia  
supone un ejercicio de creatividad permanente. ``Estética'' proviene del griego 
``aisthesis'' y quiere decir ``sensibilidad''; es un término ligado a la  
percepción de la belleza. De ``estética'' se deriva ``esteticismo'', que es  
algo así como la primacía de lo bello, o que la belleza tenga más importancia 
que cualquier otra cosa.

\item[Baudelaire] Una figura representativa del esteticismo es el dandy. Figura 
que Baudelaire describió magistralmente en el siglo XIX. El dandy es un
seductor, pero alguien que hace de la seducción no un medio, sino un fin en sí 
mismo. De este modo, el esteticismo puede implicar el consumo superficial de la 
belleza industrial del mercado, pero también puede implicar todo lo contrario.  
Hay un esteticismo que pretende liberar la belleza de sus encorsetamientos, 
postulando la figura de un dandy bien preocupado por la creación permanente de 
sí mismo, o como se decía en épocas de Baudelaire: {\em ``La mejor obra de arte 
para un artista es su propia vida``}. Pero ¿es cierto que todo se ha 
estetizado? ¿Por qué, en estos tiempos, podemos llegar a confundir cualquier 
cosa con una obra de arte? 

Hoy la estética desborda sus áreas tradicionales e inunda todo. Todo se 
encuentra estetizado: la política, la religión, la economía, la educación. Que 
el valor más importante de una clase en un colegio sea que no aburra, o que un 
político busque, antes que nada, caer bien; eso es estetización. La
estetización general de la existencia convierte cualquier acción humana en un 
acto estético. El mercado se transforma en un gran productor de belleza. Seduce 
para vender. Pero si todo es bello y se produce belleza todo el tiempo, si 
todas las cosas que nos rodean intentan generarnos una experiencia estética, 
¿qué queda del arte? ¿Queda arte? Digamos primero que sí. Queda arte, pero no 
en su forma tradicional. El arte se transforma gracias a la tecnología, que 
permite reproducirlo de forma infinita. 

\item[Walter Benjamin] teorizó sobre el tema de la reproductibilidad en el 
arte. Sostenía que el arte se modifica, por un lado, porque se socializa, y se 
vuelve más accesible para todos, perdiendo, por eso, su aura original. Sin 
embargo, la reproductibilidad permite nuevas formas de manifestación estética.  
Hace posible, gracias a la intervención tecnológica, modos de expresión, como 
el cine, que antes eran imposibles, y así como hoy podemos escuchar música en 
diferentes artefactos, en nuestro hogar o en el auto, podemos también poseer la 
copia de cualquier obra de arte sea en forma física, o sea en forma virtual.  
Pero ¿es la misma obra? ¿Gana o pierde la Gioconda?. Si bien la
reproductibilidad técnica incorpora grandes sectores sociales al mundo del 
arte, el arte se vuelve mercancía. Y entonces los criterios de producción 
artística no van a diferir de cualquier criterio de producción de mercancías.  
Esta industrialización del arte genera el {\bf \em kitsch}\footnotemark[1], ya 
que el mercado busca instalar sus productos del modo más eficiente. El {\em 
kitsch} eleva la condición de estético a cualquier cosa.  Pero,
fundamentalmente, permite la apropiación estética de cualquier objeto por 
cualquier persona, o mejor dicho, por cualquier consumidor. Por eso hay como 
una sensación de ironía permanente, como si nadie se tomara nada demasiado en 
serio, y jugara, trasgrediéndolo todo.  

\singlespace \footnotetext[1]{ La palabra kitsch se origina en el término yidis 
etwas verkitschen. Define al arte que es considerado como una copia inferior de 
un estilo existente.  También se utiliza el término kitsch en un sentido más 
libre para referirse a cualquier arte que es pretencioso, pasado de moda o de 
muy mal gusto.}

\end{description}

También es cierto que el espíritu de la vanguardia parece desaparecer como 
manifiesto político y permanecer como apuesta estética. En la idea de 
posvanguardia, el rupturismo se mantiene, pero, desprovisto de intención 
utópica, se vuelve un mero ejercicio lúdico. En un mundo estetizado, el artista 
de posvanguardia es un experimentalista. Alguien que juega y experimenta con 
las estéticas, pero vaciadas de contenido ideológico. ¿Qué queda entonces de la 
belleza? Es cierto, por un lado, con la estetización de la existencia parece 
haberse vuelto más superficial, más funcional a la sociedad de consumo.  Pero 
también es cierto que, por otro lado, hay nuevas posibilidades para reinventar 
la vida de manera más creativa. Pero el problema sigue siendo el de siempre.  
¿Qué pasa con los que se quedan afuera? El arte, en una época, quiso 
transformar la realidad. Quiso cambiar al mundo. Hoy se ha vuelto más difícil.  
Como dice un amigo mío: 

\singlespace \begin{quotation}
\em ``Cuando todo se convierte en mercancía, solo hay dos maneras de resistir: 
desde la gratuidad o desde el ridículo''.
\end{quotation}
 
\end{document}
